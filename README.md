# test-endpoints Project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:

**Quarkus: directory quarkus-endpoints requires JDK.11**

```shell script
./mvnw compile quarkus:dev

or

mvn clean package quarkus:dev
```

**Camel in Spring Boot: directory restendpoints requires JDK.8**

```shell script
mvn clean package spring-boot:run
```

Deploy similarly to quarkus with 

```shell script
mvn clean package oc:build oc:resource oc:apply -Popenshift
```

For the `restendpoints` project, document `oc adm policy add-scc-to-user anyuid -z default`

## Testing the endpoints

Four(4) endpoints are implemented by the application.

* `http://localhost:8080/hello/`, should result in "Hello World" displayed in your browser

* `http://localhost:8080/hello/xml`, should result in the pseuod-XML text `<line>Hello World</line>` displayed in your browser. You may need to "View Source" to view the full XML response as some browsers choose to hide XML elements from their display

* `http://localhost:8080/hello/data`, should result in a file named `data` being created in your download folder with a single line containing the words "Hello World"

* `http://localhost:8080/hello/file`, should result in a file named `file.dat` being created in your download folder with a single line containing the words "Hello World"

### Additional endpoints

* `http://localhost:8080/hello/createFile`, creates a new time-stamped file with whatever values were supplied in the POST data part. Example invocation : `curl -X POST http://localhost:8080/hello/createFile -d "key=value"` will just write the data `key=value` to a file in the `tmp` directory


