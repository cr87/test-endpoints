package com.example;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/hello")
public class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello World";
    }

    @GET
    @Path("/xml")
    @Produces(MediaType.APPLICATION_XML)
    public String xml() {
        return "<line>Hello World</line>";
    }

    @GET
    @Path("/data")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public String data() {
        return "Hello World";
    }

    @GET
    @Path("/file")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response fileme() {
        return Response.ok("Hello World")
                .header("content-disposition", "attachment; filename=file.dat").build();
    }
}