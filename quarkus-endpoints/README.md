### Deploy a service in HTTPS using jkube (quarkus)

First, create a development key and certificate with

```
openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem
```

Place the `key.pem` and `cert.pem` files in the `resources` folder. Add the following to `application.properties`

```
quarkus.http.ssl.certificate.file=cert.pem
quarkus.http.ssl.certificate.key-file=key.pem
```

Modify `pom.xml` to instruct jkube to create services/routes for HTTPS in `passthrough` mode with the following properties:

```XML
<properties>
  <!-- this is not needed for HTTPS, just changes the image name -->
  <jkube.generator.name>mull/craig-test-endpoint:${project.version}</jkube.generator.name>
  
  <jkube.enricher.jkube-openshift-route.tlsTermination>passthrough</jkube.enricher.jkube-openshift-route.tlsTermination>
  <jkube.enricher.jkube-openshift-route.tlsInsecureEdgeTerminationPolicy>None</jkube.enricher.jkube-openshift-route.tlsInsecureEdgeTerminationPolicy>
  <jkube.enricher.jkube-service.port>8443:8443</jkube.enricher.jkube-service.port>
  <jkube.enricher.jkube-service.expose>true</jkube.enricher.jkube-service.expose>
  
  <!-- without additional configuration only one port is exposed by default -->
  <!-- so the following would just expose 8080 -->
  <!-- <jkube.enricher.jkube-service.port>8080:8080,8443:8443</jkube.enricher.jkube-service.port> -->
  <!-- <jkube.enricher.jkube-service.multiPort>true</jkube.enricher.jkube-service.multiPort> -->
</properties>
```

Build and deploy to OCP with 

```
mvn clean package oc:build oc:resource oc:apply -Popenshift
```

Links:

* https://docs.openshift.com/container-platform/4.6/networking/routes/secured-routes.html
* https://quarkus.io/guides/http-reference#ssl
* https://access.redhat.com/documentation/en-us/openshift_container_platform/4.10/html/cicd/ci-cd-overview
* https://access.redhat.com/documentation/en-us/openshift_container_platform/4.10/html-single/cicd/index#gitops
* gist with an OCP medley: https://gist.github.com/rafaeltuelho/111850b0db31106a4d12a186e1fbc53e

