/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.restpoints.router;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class HelloRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        // @formatter:off
        // API Configuration
        restConfiguration()
            .component("servlet")
            .bindingMode(RestBindingMode.auto)
            .dataFormatProperty("prettyPrint", "true");
        
        
        rest().path("/")
            .get().produces(MediaType.TEXT_PLAIN_VALUE).route()
            .setBody(constant("Hello World - Sep23"));

        rest("/xml")
            .get().produces(MediaType.APPLICATION_XML_VALUE).route().setBody(constant("<line>Hello World - Sep23</line>"));

        rest("/data")
            .get().produces(MediaType.APPLICATION_OCTET_STREAM_VALUE).route().setBody(constant("Hello World - Sep23"));

        rest("/file")
            .get().produces(MediaType.APPLICATION_OCTET_STREAM_VALUE).route()
            .setHeader("Content-Disposition", simple("attachment;filename=file.dat"))
            .setBody(constant("Hello World - Sep23"));

        rest("/createFile")
            .post().consumes(MediaType.ALL_VALUE).produces(MediaType.TEXT_HTML_VALUE).bindingMode(RestBindingMode.auto)
            .route()
            .to("file:{{app.storeFileDirectory}}?fileName={{app.storeFileNamePrefix}}${date:now:yyyy-MM-dd-HHmmss}.txt")
            .log("createFile endpoint reached ${date:now:yyyy-MM-dd HH:mm:ss} FILE ${header.CamelFileNameProduced}")
            .setBody(constant("createFile endpoint reached"));

        // @formatter:on
    }

}
