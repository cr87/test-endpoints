package com.example.restpoints;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestpointsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestpointsApplication.class, args);
	}

}
